# setup.py
import setuptools

with open("README.md", "r") as f:
    load_description = f.read

requirements = [
    "python-dotenv",
    "selenium",
    "pymongo~=3.10"
]

test_requirements = [
    'flake8',
    'autopep8',
    'pytest',
    'pytest-watch'
]

def _requires_from_file(filename):
    return open(filename).read().splitlines()

setuptools.setup(
    name="wdlib",
    version="0.0.1",
    author="tkok",
    author_email="okawabb@gmail.com",
    description="This package contains basic features for webscraping by selenium",
    load_description=load_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/tkok/wdlib.git",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3.6",
        "Operating System :: OS Independent",
    ],
    install_requires=requirements,
    tests_require=test_requirements,
)
