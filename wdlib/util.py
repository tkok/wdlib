"""Utilities"""
import os
from datetime import datetime

from dotenv import load_dotenv

load_dotenv(verbose=True)

def get_debug() -> bool:
    """Return DEBUG const"""
    return os.getenv("DEBUG", 'False').lower() in ('true', '1', 't')

def selenium_url() -> str:
    """Return format url of selenium"""
    wd_host = os.getenv("SELENIUM_HOST")
    wd_url = "http://{}/wd/hub".format(wd_host) if wd_host else ""
    return wd_url

def save_current_page_for_debug(
    instance_of_scraping, title="", force=False
):
    """In debug mode it outputs current page html and screenshot.
        Saved name will be `title`+current_page+Ymd.(png|html).
    """
    if get_debug() or force:
        now = datetime.now()
        if title:
            title = title + "_"
        if not os.path.exists("./debug"):
            os.mkdir("./debug")

        path = "./debug/{}current_page{}".format(title, now.strftime('%Y%m%d'))
        instance_of_scraping.screen_shot(path+".png")
        instance_of_scraping.save_current_page(path=path+".html")
