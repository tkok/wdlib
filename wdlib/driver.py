"""
Handle webdriver
"""
from selenium.webdriver import ChromeOptions, Remote
from selenium.webdriver.remote.webdriver import WebDriver

class Driver(object):
    """Basement class for webdriver. it mainly implements basic actions for
    webdriver initialize and launching.
    Second arg `debug` in constructor activates browser visualization (means
    without headless option) which helps debug with window for selenium server.
    """

    def __init__(self, wd_url="", debug=True):
        self.__wd_url = wd_url or "http://localhost:4444/wd/hub"
        self.driver = None
        self.debug = debug
        self._initialize_chrome_options()

    def __del__(self):
        """
        If driver is active when instance destructs,
        it should release webdriver instance.
        """
        if hasattr(self.driver, "quit") and \
                callable(getattr(self.driver, "quit")):
            print("del")
            self.driver.close()
            self.driver.quit()

    def _initialize_chrome_options(self):
        self.options = ChromeOptions()
        if not self.debug:
            self.set_chrome_option("--headless")
        self.set_chrome_option("--disable-gpu")
        # self.set_chrome_option("--window-size=1000,1000")

    def set_chrome_option(self, argument: str):
        self.options.add_argument(argument)

    def launch(self) -> 'WebDriver':
        """
        Launch webdriver with remote selenium server.
        If selenium url error or chrome options contains errors,
        It will throw exception error by `selenium.webdriver.Remote`.
        """
        self.driver = Remote(
            command_executor=self.__wd_url, options=self.options
        )

        return self.driver
