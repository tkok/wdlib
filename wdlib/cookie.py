"""Cookie management"""
import datetime
import os
import pickle

from dotenv import load_dotenv

import pymongo
from pymongo import MongoClient

from selenium.webdriver.remote.webdriver import WebDriver

load_dotenv(verbose=True)

class Cookie(object):
    """Cookie class handles webdriver cookie.
    webdriver owns browser cookie which is assigned by web site.
    yahoo recognize the browser status by stored cookie.
    webdriver usually reset it when to launch that is not efficient
    for webdriver operation, especially reset logged-in condition.
    To avoid it Cookie class implements methods to load and store
    in arbitrary step.
    """
    date_key = "created"

    def __init__(self, instance_of_driver: 'WebDriver', key: str):
        assert type(instance_of_driver).__name__ == 'WebDriver'
        self.wd = instance_of_driver
        self.__key = key
        self.__init_mongo()

    def __del__(self):
        # mongo disconnect
        self.conn.close()

    def __init_mongo(self):
        """Connection for Mongo and initialize TTL collection"""
        host = os.getenv("MONGO_HOST")
        port = int(os.getenv("MONGO_PORT"))
        db = os.getenv("MONGO_DB_COOKIE")
        col = os.getenv("MONGO_COLLECTION_COOKIE")
        self.conn = MongoClient(host, port)

        assert self.conn is not None

        database = self.conn[db]
        self.col = database[col]
        # Set expired index option
        self.sec_expire = int(os.getenv("COOKIE_EXPIRE_SECONDS"))
        self.col.create_index(self.date_key, expireAfterSeconds=self.sec_expire)

    def save(self) -> 'pymongo.results.InsertOneResult':
        """Save current cookie into db"""
        self.remove()
        rec = {
            "key": self.__key,
            "cookies": pickle.dumps(self.wd.get_cookies()),
            self.date_key: datetime.datetime.utcnow()
        }
        return self.col.insert_one(rec)

    def load(self) -> bool:
        """Load saved cookie and set it to driver"""
        doc = self.col.find_one({"key": self.__key})
        if doc:
            cookies = pickle.loads(doc["cookies"])
            self.wd.delete_all_cookies()
            for cookie in cookies:
                self.wd.add_cookie(cookie)

            return True
        else:
            return False

    def remove(self) -> None:
        """Remove target cookie in db"""
        self.col.delete_one({"key": self.__key})
