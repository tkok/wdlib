# Basement class for scraping
import random
import re
import time

from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

class Scraping(object):
    wait_time = 10

    def __init__(self, instance_of_driver: 'WebDriver'):
        assert type(instance_of_driver).__name__ == 'WebDriver'
        self.wd = instance_of_driver
        self.rand_max_sleep = 5

    def rand_sleep(self, max_sec=0):
        if max_sec == 0:
            max_sec = self.rand_max_sleep
        sec = random.randint(1, max_sec)
        time.sleep(sec)

    def access(self, url: str):
        """Access target `url`"""
        self.wd.get(url)
        self.rand_sleep()

    def set_full_window(self):
        """set browser window size to full available size"""
        w = self.wd.execute_script("return document.body.scrollWidth;")
        h = self.wd.execute_script("return document.body.scrollHeight;")
        self.wd.set_window_size(w, h)

    def screen_shot(self, img_path: str):
        """Take screen shot"""
        self.wd.save_screenshot(img_path)

    def current_page(self) -> str:
        """Extract current page as html"""
        return self.wd.page_source

    def save_current_page(self, path=""):
        """Extract current page and save file as `path` name"""
        d = self.current_page()
        with open(path, mode='w') as f:
            f.write(d)

    def wait_until_visible_by(self, by: str, value: str) -> 'WebElement':
        """Common procedure to wait target visible element by `by`
            and return found webelement.
            Args:
                by (str): as By.ID, By.CSS_SELECTOR etc
                value (str): Correspond value for key
        """
        return self.wait_until_condition_by(
            "visibility_of_element_located", by, value
        )

    def wait_until_condition_by(
        self, condition: str, by: str, value: str
    ) -> 'WebElement':
        """Common procedure to wait target `expect_method` `by` `value`
            and return found webelement.
            Args:
                condition (str): as visibility_of_element_located,
                                 element_to_be_clickable etc.
                by (str): as By.ID, By.CSS_SELECTOR etc
                value (str): Correspond value for `by`
        """
        method = getattr(EC, condition)
        return WebDriverWait(self.wd, self.wait_time).until(method((by, value)))

    def confirm_elem_with_condition_and_text(
        self, condition: str, by: str, value: str,
        pattern: str, err_message=""
    ) -> 'WebElement':
        try:
            elem = self.wait_until_condition_by(condition, by, value)
        except Exception:
            raise Exception(err_message)

        search = re.search(pattern, elem.text)
        if search:
            return elem
        else:
            return None

    def reset_wait_time(self):
        self.wait_time = 10
