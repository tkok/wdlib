"""Test cookie module"""
from unittest.mock import MagicMock, Mock

import pytest

from wdlib.cookie import Cookie
from wdlib.cookie import MongoClient

class WebDriver(object):
    def get_cookies(self): pass

@pytest.fixture(autouse=True)
def mongopatch(mocker):
    col = MagicMock()
    col.delete_one = MagicMock()
    col.insert_one = MagicMock()
    class conn:
        def __getitem__(self, item):
            return {
                "test_col": col
            }

        def close(self):
            return MagicMock()

    mocker.patch("wdlib.cookie.MongoClient", return_value=conn())
    mocker.patch.dict("wdlib.cookie.os.environ", 
    {
        "MONGO_HOST":"a",
        "MONGO_PORT":"1",
        "MONGO_DB_COOKIE":"test_db",
        "MONGO_COLLECTION_COOKIE":"test_col",
    }
    )
    mocker.patch("wdlib.cookie.pickle")
    return col

def test_remove(mongopatch):
    c = Cookie(WebDriver(), "test_key")
    c.remove()

    mongopatch.delete_one.assert_called()
    mongopatch.delete_one.assert_called_with(
        {"key": "test_key"}
        )

def test_save(mongopatch):
    """Save call self.remove() internally."""
    c = Cookie(WebDriver(), "test_key")
    c.save()
    mongopatch.delete_one.assert_called()