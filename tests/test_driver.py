from selenium.webdriver import ChromeOptions

from wdlib.driver import Driver

def test_driver_constructor(mocker):
    d = Driver()
    wdurl = d._Driver__wd_url
    assert wdurl == "http://localhost:4444/wd/hub"
    assert type(d.options) == type(ChromeOptions())

    mocker.patch('wdlib.driver.Driver.launch', return_value=1)
    res = d.launch()
    assert res == 1


